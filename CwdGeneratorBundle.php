<?php
/*
 * This file is part of CWD Generator Bundle
 *
 * (c)2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\GeneratorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class cwdGeneratorBundle
 * @package Cwd\GeneratorBundle
 * @author Ludwig Ruderstaler <lr@cwd.at>
 */
class CwdGeneratorBundle extends Bundle
{
}
