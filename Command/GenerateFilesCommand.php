<?php
/*
 * This file is part of CWD Generator Bundle
 *
 * (c)2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\GeneratorBundle\Command;

use Cwd\GeneratorBundle\Service\ClassGenerator;
use Doctrine\ORM\Tools\Console\Command\SchemaTool\AbstractCommand;
use Sensio\Bundle\GeneratorBundle\Command\GeneratorCommand;
use Sensio\Bundle\GeneratorBundle\Command\Validators;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;

/**
 * Default Command.
 *
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class GenerateFilesCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    public function configure()
    {
        $this
            ->setDefinition(array(
                new InputOption(
                    'controller',
                    '',
                    InputOption::VALUE_REQUIRED,
                    'The name of the controller to create'
                ),
                new InputOption(
                    'namespace',
                    '',
                    InputOption::VALUE_REQUIRED,
                    'The name of the controller to create',
                    'App'
                ),
            ))
            ->setDescription('Generates default classes for entity')
            ->setHelp('')
            ->setName('cwd:generate:classes');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        if (null === $input->getOption('controller')) {
            throw new \RuntimeException('The controller option must be provided.');
        }

        $generator = $this->createGenerator();

        $generator->generateController(
            $input->getOption('namespace'),
            $input->getOption('controller')
        );

        $output->writeln('Generating the bundle code: <info>OK</info>');
    }


    protected function createGenerator()
    {
        return new ClassGenerator($this->getContainer()->get('filesystem'));
    }
}
