<?php
/*
 * This file is part of CWD Generator Bundle
 *
 * (c)2016 cwd.at GmbH <office@cwd.at>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Cwd\GeneratorBundle\Service;

use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Filesystem\Filesystem;

/**
 * CWD Class Generator
 * Based on the SensioGeneratorBundle
 * @author Fabien Potencier <fabien@symfony.com>
 * @author  Ludwig Ruderstaller <lr@cwd.at>
 */
class ClassGenerator
{
    private $filesystem;
    private $skeletonDirs;
    private static $output;


    /**
     * Constructor.
     *
     * @param Filesystem $filesystem A Filesystem instance
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
        $this->setSkeletonDirs();
    }

    /**
     * @param array $skeletonDirs An array of skeleton dirs
     */
    public function setSkeletonDirs()
    {
        $skeletonDirs = array();

        $skeletonDirs[] = __DIR__.'/../Resources/skeleton';
        $skeletonDirs[] = __DIR__.'/../Resources';

        $this->skeletonDirs = $skeletonDirs;
    }

    /**
     * @param string          $namespace
     * @param string          $controller
     * @param string          $templatePath
     *
     * @return string
     */
    public function generateController(
        $namespace,
        $controller
    ) {
        $controllerFile = 'src/Infrastructure/Web/Controller/'.$controller.'Controller.php';
        $exceptionFile = 'src/Domain/'.$controller.'/Exception/'.$controller.'NotFoundException.php';
        $managerFile = 'src/Domain/'.$controller.'/'.$controller.'Manager.php';
        $repositoryFile = 'src/Domain/'.$controller.'/'.$controller.'Repository.php';
        $testFile = 'tests/'.$controller.'/'.$controller.'ManagerTest.php';

        $gridFile = 'src/Infrastructure/Web/Grid/'.$controller.'Grid.php';
        $formFile = 'src/Infrastructure/Web/Form/'.$controller.'Type.php';

        $parameters = array(
            'namespace' => $namespace,
            'type' => $controller,
            'controller' => $controller,
        );

        $this->renderFile('Controller.php.twig', $controllerFile, $parameters);
        $this->renderFile('Exception.php.twig', $exceptionFile, $parameters);
        $this->renderFile('Manager.php.twig', $managerFile, $parameters);
        $this->renderFile('Repository.php.twig', $repositoryFile, $parameters);
        $this->renderFile('Form.php.twig', $formFile, $parameters);
        $this->renderFile('Grid.php.twig', $gridFile, $parameters);
        $this->renderFile('Tests/Test.php.twig', $testFile, $parameters);
    }


    protected function render($template, $parameters)
    {
        $twig = $this->getTwigEnvironment();

        return $twig->render($template, $parameters);
    }

    /**
     * Gets the twig environment that will render skeletons.
     *
     * @return \Twig_Environment
     */
    protected function getTwigEnvironment()
    {
        return new \Twig_Environment(new \Twig_Loader_Filesystem($this->skeletonDirs), array(
            'debug' => true,
            'cache' => false,
            'strict_variables' => true,
            'autoescape' => false,
        ));
    }

    protected function renderFile($template, $target, $parameters)
    {
        self::mkdir(dirname($target));

        return self::dump($target, $this->render($template, $parameters));
    }

    /**
     * @internal
     */
    public static function mkdir($dir, $mode = 0777, $recursive = true)
    {
        if (!is_dir($dir)) {
            mkdir($dir, $mode, $recursive);
            self::writeln(sprintf('  <fg=green>created</> %s', self::relativizePath($dir)));
        }
    }

    /**
     * @internal
     */
    public static function dump($filename, $content)
    {
        if (file_exists($filename)) {
            self::writeln(sprintf('  <fg=yellow>updated</> %s', self::relativizePath($filename)));
        } else {
            self::writeln(sprintf('  <fg=green>created</> %s', self::relativizePath($filename)));
        }

        return file_put_contents($filename, $content);
    }


    private static function writeln($message)
    {
        if (null === self::$output) {
            self::$output = new ConsoleOutput();
        }

        self::$output->writeln($message);
    }

    private static function relativizePath($absolutePath)
    {
        $relativePath = str_replace(getcwd(), '.', $absolutePath);

        return is_dir($absolutePath) ? rtrim($relativePath, '/').'/' : $relativePath;
    }
}
